<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5yHmaxIPXrbp8tQwzoV1o77UlRfiFQJ8U3vwVkOl9CZMTdX6SH/QQl/2vd6ZThxkr9TB9lrB29415+S/O+cCpw==');
define('SECURE_AUTH_KEY',  'd2t1rbZOOFCNwb2Avg1yad5wVAuAhIhsoRjkrAg8NKRLAIeV8arq2/4BOHUj4grffEqvDqCxtV9nYB0GqhoSzQ==');
define('LOGGED_IN_KEY',    'J/qW0WvRYCLSiMxh3Jq48l8aE31w1sHkISvHAr/5Ljr8/VQLmbq2mrN1QIRL8rv36qxUJDDmoe5t2HuzzxHowQ==');
define('NONCE_KEY',        'EMNrTGH31bgUPUnTsMhFtB0+ZeV+p7bIv6ig7DvFO2yy4gh1YQ1ixssW2NuoeZf4E/sb0gPJF5YfsL464s3mnQ==');
define('AUTH_SALT',        'v93etC/r546ger0t6LyMtXvhdWDrWSp0E6uLEZxQ5RDjNss+zTY7sGuEkORETUT/QZwONx5tPKdHKD7AYmVQqg==');
define('SECURE_AUTH_SALT', 'xGxyScVYeJs0qCpNT4v/T7mT5aW1dd+eeaq/RO7rQas556u9TTLoeTXzhdb3tKEkmfklWKoX9N2X1EgjjLyziQ==');
define('LOGGED_IN_SALT',   'PgKHTgcv6J++0E4NvNn2sR8kInN6ESsnwOqdStDadvkmy7wD7BLlTAz7Y8R4CVmD9oo/ypce0dDfVPstWiOXEw==');
define('NONCE_SALT',       'yf6jICjGLPzDL3UTFwP81HE7GE2XB+pBnewRTdYW+X9I/9JP8rACfF0frQvFI93huihpjTydsawxv+bLN6yjSA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
