<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo( 'description' ) ?>">
    <?php wp_head() ?>
</head>
<body <?php body_class(); ?> >
    <?php wp_body_open() ?>
    <header>
        <?php 
            $args = array(
                'menu' => 'navegacao',
                'container' => 'nav'
            );
            wp_nav_menu( '$args' ) 
        ?>
    </header> 