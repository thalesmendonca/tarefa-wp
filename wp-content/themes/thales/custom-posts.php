<?php
  /*
    Template Name: Custom Posts
  */  
?>


<?php get_header() ?>

<main>
  <h1>Custom Posts</h1>
  <?php  
    $args = array(
      'post_type'       => 'noticia',
      'post_status'     => 'publish',
      'supress_filters' => true,
      'ordery'          => 'post_date',
      'order'           => 'DESC'
    );
    $news = new WP_Query($args);
     
  ?>
  <?php the_content( ) ?>
  <form id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="text" class="search-field" name="s" placeholder="Search" value="<?php echo get_search_query(); ?>">
    <input type="hidden" name="post_type[]" value="noticia" />
    <input type="submit" value="Search">
  </form>
  <?php 
    //get_search_form();
    if ( $news -> have_posts() ) : while ( $news -> have_posts() ) : $news -> the_post(); ?>
      <h2><?php the_title() ?></h2>
      <a href="<?php the_permalink()?> ">link</a>
    <?php endwhile; else : ?>
      <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php 
      endif; 
      $big = 99999999;
      echo paginate_links(
        array(
          'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
          'format'  => '?paged=%#%',
          'current' => max( 1, get_query_var('paged') ),
          'total'   => $news->max_num_pages
        )
        );
      wp_reset_postdata( );?>

</main>

<?php get_footer() ?>
