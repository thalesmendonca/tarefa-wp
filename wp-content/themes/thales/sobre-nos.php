<?php
  /*
    Template Name: Sobre Nós
  */  
?>


<?php get_header() ?>

    <main>
        <h1> <?php the_title( ) ?> </h1>
        <?php the_content( ) ?>
        <p><?php the_field('texto') ?></p>
        <?php get_template_part('inc','section') ?>
    </main>

<?php get_footer() ?>