<?php get_header() ?>

    <main>
    <h1>Pesquisa</h1>
      <?php the_content( ) ?>
      <?php 
        get_search_form();
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <h2><?php the_title() ?></h2>
          <a href="<?php the_permalink()?> ">link</a>
        <?php endwhile; else : ?>
          <p><?php esc_html_e( 'Não temos posts' ); ?></p>
        <?php 
          endif; 
          echo paginate_links()
      ?>

    </main>

<?php get_footer() ?>