<?php
  /*
    Template Name: Posts
  */  
?>


<?php get_header() ?>

    <main>

      <h1>Posts</h1>
      <form id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
      <input type="text" class="search-field" name="s" placeholder="Search" value="<?php echo get_search_query(); ?>">
      <input type="hidden" name="post_type[]" value="post" />
      <input type="submit" value="Search">
      </form>
      <?php 
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <h2><?php the_title() ?></h2>
          <a href="<?php the_permalink()?> ">link</a>
        <?php endwhile; else : ?>
          <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php 
          endif; 
          echo paginate_links()
      ?>

    </main>

<?php get_footer() ?>