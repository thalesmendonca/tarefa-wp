<?php 
    /*
        Template Name: Home Page
    */
?>


<?php get_header() ?>
    <main>
        <h1>Home Page</h1>
        <?php $image = (get_field('imagem')) ?>
        <img  src="<?php echo $image ?>" alt="">
        <p><?php echo the_field('campo_de_texto') ?></p>
        <p><?php echo the_field('area_de_texto') ?></p>
        <?php 
            get_template_part( 'inc', 'section')
        ?>
    </main>

<?php get_footer() ?>